module gitlab.com/arnef/coronaapp

go 1.13

require (
	github.com/disintegration/imaging v1.6.2
	github.com/leonelquinteros/gotext v1.5.0
	github.com/makiuchi-d/gozxing v0.1.1
	github.com/nanu-c/qml-go v0.0.0-20201002212753-238e81315528
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/arnef/covcert v0.0.0-20221016152315-04edc47c86ae
)
