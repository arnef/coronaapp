package viewmodel

import (
	"fmt"
	"time"
)

type Cert struct {
	ID          string
	FullName    string
	DateOfBirth string
	Data        *DataRows
	ValidUntil  time.Time
	//
	Title     string
	SubTitle  string
	Icon      string
	Color     string
	TextColor string
}

func intl(val string, key string) string {
	if val != key {
		return fmt.Sprintf("%s / %s", val, key)
	}
	return val
}
