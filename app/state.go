package app

import (
	"strings"

	"github.com/leonelquinteros/gotext"
	"github.com/nanu-c/qml-go"
	"gitlab.com/arnef/coronaapp/app/storage"
	"gitlab.com/arnef/coronaapp/app/viewmodel"
	"gitlab.com/arnef/covcert/pkg/decoder"

	log "github.com/sirupsen/logrus"
)

type State struct {
	Root  qml.Object
	Certs *viewmodel.CertList
}

func (s *State) Init() {
	log.Debug("init state")
	files, err := storage.ReadDir("")

	if err == nil {
		for _, f := range files {
			if strings.HasSuffix(f, ".pem") {
				log.Debug(f)
				data, err := storage.ReadFile(f)
				if err == nil {
					// cert, err := utils.CertFromString((string(data)))
					cert, err := decoder.DecodeString(string(data))
					if err != nil {
						log.Error(err)
						continue
					}

					s.Certs.Append(cert)

				}
			}
		}
	} else {
		log.Error(err)
	}
	if s.Certs.Size == 0 {
		s.Certs.EmptyMessage = gotext.Get("You currently do not have saved any digital EU-COVID certificates.")
	}
	qml.Changed(s, &s.Certs)

}
