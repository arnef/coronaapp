# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: EMAIL\n"
"POT-Creation-Date: 2021-09-23 16:16+0200\n"
"PO-Revision-Date: 2021-09-23 16:34+0200\n"
"Last-Translator: Heimen Stoffels <vistausss@fastmail.com>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: app/viewmodel/viewmodel.go:49 app/viewmodel/viewmodel.go:117
#, c-format
msgid "Valid until %s"
msgstr "Geldig tot %s"

#: app/viewmodel/recovery.go:11
msgid "Disease or agent the citizen has recovered from"
msgstr "Hersteld van"

#: app/viewmodel/recovery.go:23
msgid "Certificate valid from (YYYY-MM-DD)"
msgstr "Het certificaat is geldig van (JJJJ-MM-DD)"

#: app/viewmodel/viewmodel.go:116
msgid "Recovery certificate"
msgstr "Herstelcertificaat"

#: app/viewmodel/vaccination.go:26
msgid "Manufacturer"
msgstr "Fabrikant"

#: app/viewmodel/test.go:15
msgid "Type of test"
msgstr "Soort test"

#: app/viewmodel/viewmodel.go:101
msgid "Incomplete vaccination protection"
msgstr "Onvolledige bescherming"

#: app/viewmodel/viewmodel.go:106
msgid "Full vaccination protection"
msgstr "Volledige bescherming"

#: app/viewmodel/viewmodel.go:130
msgid "PCR test"
msgstr "PCR-test"

#: app/viewmodel/vaccination.go:22
msgid "Vaccine Type"
msgstr "Soort vaccinatie"

#: main.go:47
msgid "Delete certificate?"
msgstr "Wil je het certificaat verwijderen?"

#: app/viewmodel/viewmodel.go:67
msgid "Name, first name"
msgstr "Achternaam, voornaam"

#: app/viewmodel/viewmodel.go:71
msgid "Date of birth (YYYY-MM-DD)"
msgstr "Geboortedatum (JJJJ-MM-DD)"

#: app/viewmodel/viewmodel.go:75
msgid "Certificate issuer"
msgstr "Certificaatverstrekker"

#: app/viewmodel/viewmodel.go:99
msgid "Vaccination certificate"
msgstr "Vaccinatiecertificaat"

#: app/viewmodel/vaccination.go:30
msgid "Number in a series of vaccinations/doses"
msgstr "Aantal keer gevaccineerd/Aantal doseringen"

#: app/viewmodel/viewmodel.go:128
msgid "Rapid antigen test"
msgstr "Coronasneltest"

#: app/viewmodel/recovery.go:15
msgid "Date of first positive test result (YYYY-MM-DD)"
msgstr "Datum van eerste positieve testresultaat (JJJJ-MM-DD)"

#: app/viewmodel/test.go:27
msgid "Date and time of the sample collection (YYYY-MM-DD, HH:MM)"
msgstr "Datum en tijd van antistoffentest (JJJJ-MM-DD, UU:MM)"

#: app/state.go:23
msgid "You currently do not have saved any digital EU-COVID certificates."
msgstr "Je hebt nog geen opgeslagen EU-COVID-certificaten."

#: app/viewmodel/vaccination.go:18
msgid "Vaccine"
msgstr "Vaccin"

#: app/viewmodel/vaccination.go:38
msgid "Member State of vaccination"
msgstr "Gevaccineerd in"

#: app/viewmodel/test.go:35
msgid "Testing centre or facility"
msgstr "Onderzoekscentrum of -instelling"

#: main.go:49
msgid "Scan..."
msgstr "Scannen…"

#: app/viewmodel/viewmodel.go:48
msgid "Certificate"
msgstr "Certificaat"

#: app/viewmodel/vaccination.go:42 app/viewmodel/recovery.go:27
#: app/viewmodel/test.go:43
msgid "Unique certificate identifier"
msgstr "Uniek certificaatnummer"

#: app/viewmodel/recovery.go:19 app/viewmodel/test.go:39
msgid "Member State of test"
msgstr "Getest in"

#: app/viewmodel/test.go:31
msgid "Test result"
msgstr "Testresultaat"

#: app/viewmodel/vaccination.go:34
msgid "Date of vaccination (YYYY-MM-DD)"
msgstr "Vaccinatiedatum (JJJJ-MM-DD)"

#: main.go:46
msgid "Delete"
msgstr "Verwijderen"

#: main.go:48
msgid "Cancel"
msgstr "Annuleren"

#: app/viewmodel/viewmodel.go:79
msgid "Valid until (YYYY-MM-DD)"
msgstr "Geldig tot (JJJJ-MM-DD)"

#: app/viewmodel/viewmodel.go:111
#, c-format
msgid "Full protection as of %s"
msgstr "Volledig beschermd sinds %s"

#: app/viewmodel/vaccination.go:14 app/viewmodel/test.go:11
msgid "Disease or agent targeted"
msgstr "Gevaccineerd tegen"

#~ msgid "Test name"
#~ msgstr "Naam van test"

#~ msgid "Test manufacturer"
#~ msgstr "Fabrikant van test"

#~ msgid "Certificate valid until (no more than 180 days after the date of first positive test result)(YYYY-MM-DD)"
#~ msgstr "Het certificaat is geldig tot (niet langer dan 180 dagen na het eerste positieve testresultaat - JJJJ-MM-DD)"
