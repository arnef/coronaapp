# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: EMAIL\n"
"POT-Creation-Date: 2021-09-23 16:16+0200\n"
"PO-Revision-Date: 2021-09-23 16:33+0200\n"
"Last-Translator: Anne017\n"
"Language-Team: \n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n==1 ? 0 : 1);\n"
"X-Generator: Poedit 3.0\n"

#: app/viewmodel/viewmodel.go:49 app/viewmodel/viewmodel.go:117
#, c-format
msgid "Valid until %s"
msgstr "Valide jusqu'au %s"

#: app/viewmodel/recovery.go:11
msgid "Disease or agent the citizen has recovered from"
msgstr "Maladie ou agent dont le citoyen est guéri"

#: app/viewmodel/recovery.go:23
msgid "Certificate valid from (YYYY-MM-DD)"
msgstr "Certificat valide à partir du (AAAA-MM-JJ)"

#: app/viewmodel/viewmodel.go:116
msgid "Recovery certificate"
msgstr "Certificat de guérison"

#: app/viewmodel/vaccination.go:26
msgid "Manufacturer"
msgstr "Fabricant"

#: app/viewmodel/test.go:15
msgid "Type of test"
msgstr "Type de test"

#: app/viewmodel/viewmodel.go:101
msgid "Incomplete vaccination protection"
msgstr "Protection de vaccination incomplète"

#: app/viewmodel/viewmodel.go:106
msgid "Full vaccination protection"
msgstr "Protection de vaccination complète"

#: app/viewmodel/viewmodel.go:130
msgid "PCR test"
msgstr "Test PCR"

#: app/viewmodel/vaccination.go:22
msgid "Vaccine Type"
msgstr "Type de vaccin"

#: main.go:47
msgid "Delete certificate?"
msgstr "Supprimer le certificat ?"

#: app/viewmodel/viewmodel.go:67
msgid "Name, first name"
msgstr "Nom, prénom"

#: app/viewmodel/viewmodel.go:71
msgid "Date of birth (YYYY-MM-DD)"
msgstr "Date de naissance (AAAA-MM-JJ)"

#: app/viewmodel/viewmodel.go:75
msgid "Certificate issuer"
msgstr "Fournisseur du certificat"

#: app/viewmodel/viewmodel.go:99
msgid "Vaccination certificate"
msgstr "Certificat de vaccination"

#: app/viewmodel/vaccination.go:30
msgid "Number in a series of vaccinations/doses"
msgstr "Numéro dans une série de vaccinations/doses"

#: app/viewmodel/viewmodel.go:128
msgid "Rapid antigen test"
msgstr "Test antigénique rapide"

#: app/viewmodel/recovery.go:15
msgid "Date of first positive test result (YYYY-MM-DD)"
msgstr "Date du résultat du premier test positif (AAAA-MM-JJ)"

#: app/viewmodel/test.go:27
msgid "Date and time of the sample collection (YYYY-MM-DD, HH:MM)"
msgstr "Date et heure du prélèvement (AAAA-MM-JJ, HH:MM)"

#: app/state.go:23
msgid "You currently do not have saved any digital EU-COVID certificates."
msgstr "Vous n'avez sauvegardé aucun certificat numérique COVID-UE."

#: app/viewmodel/vaccination.go:18
msgid "Vaccine"
msgstr "Vaccin"

#: app/viewmodel/vaccination.go:38
msgid "Member State of vaccination"
msgstr "État membre de la vaccination"

#: app/viewmodel/test.go:35
msgid "Testing centre or facility"
msgstr "Centre de test ou assimilé"

#: main.go:49
msgid "Scan..."
msgstr "Scanner..."

#: app/viewmodel/viewmodel.go:48
msgid "Certificate"
msgstr "Certificat"

#: app/viewmodel/vaccination.go:42 app/viewmodel/recovery.go:27
#: app/viewmodel/test.go:43
msgid "Unique certificate identifier"
msgstr "Identifiant unique de certificat"

#: app/viewmodel/recovery.go:19 app/viewmodel/test.go:39
msgid "Member State of test"
msgstr "État membre du test"

#: app/viewmodel/test.go:31
msgid "Test result"
msgstr "Résultat du test"

#: app/viewmodel/vaccination.go:34
msgid "Date of vaccination (YYYY-MM-DD)"
msgstr "Date de vaccination (AAAA-MM-JJ)"

#: main.go:46
msgid "Delete"
msgstr "Supprimer"

#: main.go:48
msgid "Cancel"
msgstr "Annuler"

#: app/viewmodel/viewmodel.go:79
msgid "Valid until (YYYY-MM-DD)"
msgstr "Valide jusqu'au (AAAA-MM-JJ)"

#: app/viewmodel/viewmodel.go:111
#, c-format
msgid "Full protection as of %s"
msgstr "Protection pleine à partir de %s"

#: app/viewmodel/vaccination.go:14 app/viewmodel/test.go:11
msgid "Disease or agent targeted"
msgstr "Maladie ou agent ciblé"

#~ msgid "Test name"
#~ msgstr "Nom du test"

#~ msgid "Test manufacturer"
#~ msgstr "Fabricant du test"

#~ msgid "Certificate valid until (no more than 180 days after the date of first positive test result)(YYYY-MM-DD)"
#~ msgstr "Certificat valide jusqu'au (pas plus de 180 jours au delà de la date du premierrésultat de test positif)(AAAA-MM-JJ)"
